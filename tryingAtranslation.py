import easyocr
import ollama

reader = easyocr.Reader(['ch_sim','en']) # this needs to run only once to load the model into memory
#result = reader.readtext('ocr_me.png')
#result = reader.readtext('harder.png')
result = reader.readtext('bijiaoshirun2.png')
#print(type(result))
#print(*result, sep = "\n")
#print(result[1])
transformed_result = ''.join(map(str, result)).split(',')
ItemPos = len(transformed_result) - 2
if(ItemPos<0):
    print("ItemPos negative, quitting.")
    exit(42)
cnItem = transformed_result[ItemPos]
print("here is the easyocr result that is being fed to llm: " + cnItem)

myPromptforPinyin = "Translate the following words into their Pinyin equivalent: " + cnItem
myPromptforEnglish = "translate the following chinese sentence into english please: " + cnItem

#finalResult = ollama.generate(model='yi', prompt=myPrompt) #generate generates a complicated tuple to be extracted as a printable result, so I go with ollama.chat()

def askOllama(prompt):
    pinyinResult = ollama.chat(model='yi', messages=[
           {
                    'role': 'system',
                    'content': "give only result expected, be concise and precise."
           },
           {
                    'role': 'user',
                    'content': prompt,
           }])
    return pinyinResult['message']['content']

print("trying to get the pinyin literal translation...")
res = askOllama(myPromptforPinyin)
print(res)

print("trying to get the English llm interpreted translation...")
res = askOllama(myPromptforEnglish)
print(res)
